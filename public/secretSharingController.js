var app = angular.module('secretSharing', []);
app.config(['$compileProvider',
function ($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):|data:/);
}]);

app.directive('resize', function ($window) {
    return function (scope) {
        scope.charLimit =  Math.floor($window.innerWidth/18);
        angular.element($window).bind('resize', function () {
            scope.$apply(function () {
                scope.width = $window.innerWidth;
                scope.charLimit =  Math.floor($window.innerWidth/18);
            });
        });
    };
});

app.controller("myCtrl", function ($scope, $timeout) {

    // ================
    // Default Settings
    // ================
    //

    $scope.settings = {
        radix: 16, // the radix used when generating and combining shares
        size: 0xffff, // this is the max number that is representable by the radix used and length e.g. radix: 2, length: 3 => 2^3 -1 => 7
        length: 4, // number of letters used to represent each secret letter
        prime_p: 65521 // must be a prime < size for fixed length - used for finite field arithmetic so that the polynomial is of form: f(x) mod p
    }

    const usablePrimes = [179057, 104729, 98047, 85717, 72367, 65521, 58907, 51637, 46649, 42139, 35999, 29611, 22769, 16103, 13337, 9929, 8431,
        6863, 5717, 4093, 3847, 3469, 3023, 2713, 2311, 1951, 1549, 1291, 1021, 773, 509, 373, 251, 241]; // chosen conveniently for when changing settings

    $scope.supported = $scope.settings.prime_p;
    $scope.bases = ['Binary', 'Ternary', 'Octal', 'Dec', 'Hex', 'Base36'];
    $scope.chosenBase = 'Hex';
    $scope.lengths = [2, 3, 4];
    $scope.chosenLength = '4';

    $scope.otherBases = [];
    // adding other bases
    for(var i = 4; i < 36; i++) {
        if (i !== 16 && i !== 8 && i !== 10) {
            $scope.otherBases.push(i);
        }
    }

    $scope.setLength = function() {
        switch($scope.chosenBase) {
            case 'Binary':
                $scope.settings.radix = 2;
                $scope.lengths = [8, 10, 12, 16];
                $scope.chosenLength = '8';
                $scope.setConditions();
                break;
            case 'Ternary':
                $scope.settings.radix = 3;
                $scope.lengths = [5, 6, 7, 8, 9, 10, 11];
                $scope.chosenLength = '5';
                $scope.setConditions();
                break;
            case 'Octal':
                $scope.settings.radix = 8;
                $scope.lengths = [3, 4, 5, 6];
                $scope.chosenLength = '3';
                $scope.setConditions();
                break;
            case 'Dec':
                $scope.settings.radix = 10;
                $scope.lengths = [3, 4, 5];
                $scope.chosenLength = '3';
                $scope.setConditions();
                break;
            case 'Hex':
                $scope.settings.radix = 16;
                $scope.lengths = [2, 3, 4];
                $scope.chosenLength = '4';
                $scope.setConditions();
                break;
            case 'Base36':
                $scope.settings.radix = 36;
                $scope.lengths = [2, 3, 4];
                $scope.chosenLength = '2';
                $scope.setConditions();
                break;
            default:
                $scope.settings.radix = parseInt($scope.chosenBase);
                $scope.lengths = calcLengths($scope.chosenBase);
                $scope.chosenLength = $scope.lengths[0].toString();
                $scope.setConditions();
                break;
        }
    }

    function calcLengths(base) {
        const lengths = [];
        var size = base;
        var i = 1;
        while(size < 0xffff) {
            i++;
            size *= base;
            if(size > 250) {
                lengths.push(i);
            }
        }
        return lengths;
    }

    $scope.setConditions = function() {
        $scope.settings.length = parseInt($scope.chosenLength);
        $scope.settings.size = Math.pow($scope.settings.radix, $scope.settings.length);
        $scope.settings.prime_p = usablePrimes.find((prime) => prime < $scope.settings.size);
        $scope.supported = $scope.settings.prime_p;
    }

    const errorColor = '#943126';
    $scope.splitError = 'Please enter a secret message and how you want it to be split.';
    $scope.combineError = 'Please paste in the exact amount of shares required using a new line for each share, in the format: idx | 0123456789abcdef';
    $scope.input = {};
    $scope.calculatedShares = [];
    $scope.combineShares = '';
    $scope.combinedResult = '';
    $scope.url;

    // main split code
    $scope.split = function ({ shares: totalShares, threshold, secret }, settings) {
        if (!splitErrorCheck(totalShares, threshold, secret, settings)) return;

        var content = getFileTemplate(totalShares, threshold, secret, settings);
        secretArray = convertSecretToArray(secret, settings.prime_p);
        rndPolys = generateRandomPolynomial(secretArray, threshold, settings.prime_p);
        $scope.calculatedShares = getShares(rndPolys, threshold === 1 ? 1 : totalShares, settings);

        prepareDownloadFile(content);
        
        document.getElementById('splitErrorMsg').style.color = 'black';
        $scope.splitError = 'Secret split successfully!';
    }

    // main combine code
    $scope.combine = function (combineShares, settings) {
        $scope.combineResult = '';
        if(combineInputCheck(combineShares)) return;
        try {
            var shares = combineShares.split('\n');
            const polyPoints = convertShareToPoints(shares, settings.length, settings.radix);
            const y_intercepts = solvePolys(polyPoints, settings.prime_p);
            $scope.combineResult = displaySecret(y_intercepts);
            
            showSuccessCombineMessage();
        }
        catch(e) {
            $scope.combineError = 'ERROR: Cannot combine shares, seems to be wrong formatting...';
        }
    }

    function showSuccessCopyMessage() {
        $scope.showCopyMessage = true;
        $timeout(() => {
            $scope.showCopyMessage = false;
        }, 1000);
    }

    function showSuccessCombineMessage() {
        document.getElementById('combineErrorMsg').style.color = 'black';
        $scope.combineError = 'Secret has been combined successfully!';

        $scope.showCombineMessage = true;
        $timeout(() => {
            $scope.showCombineMessage = false;
        }, 1000);
    }

    function convertShareToPoints(shares, length, radix) {
        const polyPoints = [];

        shares.forEach((share) => {
            var parts = share.split('|');
            var x = parseInt(parts[0].trim());
            var ys = splitShare(parts[1].trim(), length, radix);
            ys.forEach((y, idx) => {
                if (!polyPoints[idx]) polyPoints[idx] = [];
                polyPoints[idx].push({ x, y });
            });
        });
        return polyPoints;
    }

    function splitShare(share,length, radix) {
        if (share.length % length !== 0) {
            $scope.combineError = 'share value length is wrong, double check the shares have been entered correctly.';
            throw Error('could not combine');
        }
        const convertedShare = [];
        for (var i = 0; i < share.length; i += length) {
            convertedShare.push(parseInt(share.substring(i, i + length), radix));
        }
        return convertedShare;
    }

    // solve via Lagrange interpolation - optimized approach as only f(0) is needed
    function solvePolys(polyPoints, prime_p) {
        const y_0s = [];
        polyPoints.forEach((points) => {
            var li = 0;
            points.forEach((point) => {
                li += calcLagrangeComponent(points, point.x, point.y, prime_p);
            });
            y_int = Math.round(li) % prime_p;
            y_0s.push(y_int < 0 ? y_int + prime_p : y_int);
        });
        return y_0s;
    }

    // calculates the x^0 component for L_i in lagrange interpolation
    function calcLagrangeComponent(points, x, y, prime_p) {
        var val = y;
        points.forEach((point) => {
            if (point.x !== x) {
                val *= (divmod(-point.x, (x - point.x), prime_p));
                val = val % prime_p;
            }
        });
        return val;
    }

    // in finite field theory, division is done by multiplying the original number with the multiplicative inverse modulo p
    function divmod(num, den, prime_p) {
        const inv = getInv(Math.abs(den), prime_p);
        return num * (den < 0 ? -inv : inv);
    }

    // extended euclidean algorithm for finding multiplicative inverse of modulo p
    //https://en.wikipedia.org/wiki/Modular_multiplicative_inverse#Computation
    function getInv(x, prime_p) {
        if (x === 1) return 1;

        var a = prime_p;
        var b = x;
        var coeff = Math.floor(a / b);
        var r = a - coeff * b;

        var euclidean = [];

        while (r !== 0) {
            euclidean.push({ a, coeff, b, r });
            a = b;
            coeff = Math.floor(a / r);
            b = r;
            r = a - coeff * r;
        }
        euclidean.reverse();

        var y_coeff = 1;
        var x_coeff = -euclidean[0].coeff;
        var x = euclidean[0].b;

        euclidean.shift();
        euclidean.forEach((euc) => {
            var old_y_coeff = y_coeff;
            y_coeff = x_coeff;
            x_coeff = old_y_coeff + (x_coeff * -euc.coeff);
            x = euc.b;
        });

        return x_coeff < 0 ? x_coeff + prime_p : x_coeff;
    }

    // converts charcode back to readable text
    function displaySecret(y_intercepts) {
        return y_intercepts.reduce((secret, y_int) => {
            return secret.concat(String.fromCharCode(y_int));
        }, '');
    }

    // returns a random number between 1 and prime_p - used for coefficients to generate random polynomials
    function getRandomNonZeroNumber(prime_p) {
        const rnd = Math.random() * (prime_p);
        const noise = new Date().getMilliseconds() + 1;
        return Math.floor(((rnd * noise) % (prime_p - 1)) + 1);
    }

    // convert each character in secret into a number, depending on what the size is set to, foreign characters might not work properly and be mapped to the mod char
    function convertSecretToArray(secret, prime_p) {
        var array = [];
        for (var i = 0; i < secret.length; ++i) {
            array.push(secret.charCodeAt(i) % prime_p);
        }
        return array;
    };

    // creates a random polynomial and sets y(0) to be the char code of the secret
    function generateRandomPolynomial(array, threshold, prime_p) {
        const polys = [];
        for (var i = 0; i < array.length; i++) {
            const poly = [array[i]];
            while (poly.length < threshold) poly.push(getRandomNonZeroNumber(prime_p));
            polys.push(poly);
        }
        return polys;
    }

    function getShares(rndPolys, totalShares, settings) {
        const shares = [];
        for (var i = 1; i <= totalShares; i++) {
            var share = '';
            for (var j = 0; j < rndPolys.length; j++) {
                share = share.concat(eval(rndPolys[j], i, settings));
            }
            shares.push(share);
        }
        return shares;
    }

    // Horner's method for calculating polynomials as it is more computationally efficient and returns f(x) as a hex string with padding
    function eval(p, x, settings) {
        var result = p.slice().reverse().reduce((total, coeff) => {
            return (coeff + total * x) % settings.prime_p;
        });
        return (result).toString(settings.radix).padStart(settings.length, 0).toUpperCase();
    }

    function splitErrorCheck(totalShares, threshold, secret, settings) {
        document.getElementById('splitErrorMsg').style.color = errorColor;
        if (!totalShares || !threshold || !secret) {
            $scope.splitError = 'ERROR: Empty field detected! Please fill in all the details before attempting to split.';
            return false;
        }
        if (threshold > totalShares) {
            $scope.splitError = 'ERROR: Total number of shares must be greater than or equal to shares.';
            return false;
        }
        if (threshold < 1) {
            $scope.splitError = 'ERROR: Shares required must be 1 or more.';
            return false;
        }
        if (totalShares < 1) {
            $scope.splitError = 'ERROR: Shares must be 1 or more.';
            return false;
        }
        if(totalShares > Math.min(25000, settings.prime_p)) {
            $scope.splitError = settings.prime_p > 25000 ? 'ERROR: Total shares limited to 25,000 to prevent crashing.' :
                `ERROR: Total shares limited to ${settings.prime_p} max supported characters.`;
            return false;
        }
        if (!secret.length) {
            $scope.splitError = 'ERROR: Secret message must contain at least one character.';
            return false;
        }
        return true;
    }

    function combineInputCheck(combineShares) {
        document.getElementById('combineErrorMsg').style.color = errorColor;
        $scope.combineError = 'ERROR: Cannot combine nothing.'
        return !combineShares.trim().length;
    }

    $scope.exampleCombine = function () {
        const share1 = '7 | 0E9CB17B8411EFEF36E3E0E93CE4B5753F3B4764C118CC92';
        const share2 = '12 | 3F6E029ACBA3A3F3F03B5AE34DDB6D74B4B356ED31F7D4F4';
        const share3 = '1 | CB39E3ADA4121A70A1C22B6AB3CF73E5FE709CACF9C8B262';
        $scope.combineShares = `${share1}\n${share2}\n${share3}`;

        $scope.chosenBase = 'Hex';
        $scope.setLength();

        $scope.combine($scope.combineShares, $scope.settings);
    }

    // copy row data to clipboard for ease of use
    $scope.copy = function (idx, share) {
        var textArea = document.createElement("textarea");
        textArea.style.position = 'fixed';
        textArea.style.bottom= 0;
        textArea.style.left= 0;
        textArea.value = `${idx + 1} | ${share}\n`;
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            document.execCommand('copy');
            showSuccessCopyMessage();
        } catch (err) {
            console.error('Fallback: Oops, unable to copy', err);
        }
        document.body.removeChild(textArea);
    }

    $scope.copyRandom = function() {
        var copyShares = $scope.calculatedShares.map((share, i) => {
            return {idx: i + 1, share};
        });
        var rndShares = [];
        for(var i = 0; i < $scope.input.threshold; i++) {
            rndShares.push(...copyShares.splice(Math.random()*copyShares.length, 1));
        }
        copyRandomToClipboard(rndShares);
    }

    function copyRandomToClipboard(rndShares) {
        var textArea = document.createElement("textarea");
        textArea.style.position = 'fixed';
        textArea.style.bottom= 0;
        textArea.style.left= 0;
        textArea.value = rndShares.reduce((copyText, share) => {
            return copyText.concat(`${share.idx} | ${share.share}\n`);
        }, '');
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            document.execCommand('copy');
            showSuccessCopyMessage();
        } catch (err) {
            console.error('Fallback: Oops, unable to copy', err);
        }
        document.body.removeChild(textArea);
    }

    function getFileTemplate(totalShares, threshold, secret, settings) {
        return `
==============================\r\n
SHAMIR'S SECRET SHARING SCHEME\r\n
==============================\r\n
Created by: Wai Get Law\r\n
Please visit: https://waigetlaw.gitlab.io/html/secretSharing.html\r\n\r\n

Total shares: ${totalShares}\r\n
Shares required: ${threshold}\r\n
Base: ${$scope.chosenBase}, ${settings.radix}\r\n
Length: ${settings.length}\r\n
Prime p: ${settings.prime_p}\r\n
Secret: ${secret}\r\n\r\n

======\r\n
SHARES\r\n
======\r\n\r\n`
    }

    function prepareDownloadFile(content) {
        $scope.calculatedShares.forEach((share, i) => {
            content = content.concat(`${i+1} | ${share} \r\n`);
        });
        var blob = new Blob([content], {type: 'text/plain'});
        $scope.url = (window.URL || window.webkitURL).createObjectURL(blob);
    }

    // split files
    $scope.fileName1 = '';
    $scope.fileName2 = '';
    $scope.fileData1 = '';
    $scope.fileData2 = '';

    $scope.uploadStatus = 'Choose a file (limited to 1.5MB~) to begin the splitting process';

    $scope.upload = function () {
        $scope.uploadStatus = 'Uploading...';
        var filesSelected = document.getElementById("fileToSplit").files;
        if (filesSelected.length > 0) {
            var fileToLoad = filesSelected[0];
            if (fileToLoad.size > 1560000) {
                $scope.uploadStatus = 'Please choose a smaller file';
                return;
            }
            $scope.fileData1 = '';
            $scope.fileData2 = '';
            var fileReader = new FileReader();

            fileReader.onloadend = function(e) {
                    const totalShares = 2;
                    const threshold = 2;
                    const settingsFileName = {
                        radix: 36,
                        size: 105000,
                        length: 4,
                        prime_p: 104729
                    }
                    var secretArray = convertSecretToArray(fileToLoad.name, settingsFileName.prime_p);
                    var rndPolys = generateRandomPolynomial(secretArray, threshold, settingsFileName.prime_p);
                    var splitFileName = getShares(rndPolys, threshold === 1 ? 1 : totalShares, settingsFileName);

                    const settingsFileData = {
                        radix: 36,
                        size: 1296,
                        length: 2,
                        prime_p: 1291
                    }
                    secretArray = convertSecretToArray(e.target.result.toString(), settingsFileData.prime_p);
                    rndPolys = generateRandomPolynomial(secretArray, threshold, settingsFileData.prime_p);
                    var splitFileData = getShares(rndPolys, threshold === 1 ? 1 : totalShares, settingsFileData);

                    $scope.fileName1 = '1_'.concat(splitFileName[0]);
                    $scope.fileName2 = '2_'.concat(splitFileName[1]);

                    var blob = new Blob(['1 | '.concat(splitFileData[0])], {type: 'text/plain'});
                    $scope.fileData1 = (window.URL || window.webkitURL).createObjectURL(blob);

                    var blob = new Blob(['2 | '.concat(splitFileData[1])], {type: 'text/plain'});
                    $scope.fileData2 = (window.URL || window.webkitURL).createObjectURL(blob);
                    
                    $scope.uploadStatus = 'Upload success!';
                    $scope.$apply();
            }
            fileReader.readAsDataURL(fileToLoad);
            return;
        }
        $scope.uploadStatus = 'No files detected';
    }

    // combine files
    $scope.combinedFileName = '';
    $scope.combinedFileData = '';

    $scope.combineStatus = 'Upload two files (limited to 5MB~) to begin the combine process';

    $scope.combineFiles = async function() {
        $scope.combineStatus = 'Combining...'
        var file1Selected = document.getElementById("fileCombine1").files;
        if(file1Selected.length === 0) {
            $scope.combineStatus = 'File missing';
            return;
        }

        var file2Selected = document.getElementById("fileCombine2").files;
        if(file2Selected.length === 0) {
            $scope.combineStatus = 'File missing';
            return;
        }

        if (file1Selected[0].size > 5000000 || file2Selected[0].size > 5000000) {
            $scope.combineStatus = 'File too large to combine';
            return;
        }

        $scope.combinedFileName = '';
        $scope.combinedFileData = '';

        var fileName1 = file1Selected[0].name;
        var content1 = await readFileContent(file1Selected[0]);
        var fileName2 = file2Selected[0].name;
        var content2 = await readFileContent(file2Selected[0]);

        const settingsFileName = {
            radix: 36,
            size: 105000,
            length: 4,
            prime_p: 104729
        }

        const settingsFileData = {
            radix: 36,
            size: 1296,
            length: 2,
            prime_p: 1291
        }

        try {
            var fileNameParts = [fileName1.split('.')[0].replace('_', ' | '), fileName2.split('.')[0].replace('_', ' | ')];
        
            var polyPoints = convertShareToPoints(fileNameParts, settingsFileName.length, settingsFileName.radix);
            var y_intercepts = solvePolys(polyPoints, settingsFileName.prime_p);
            var realFileName = displaySecret(y_intercepts);

            $scope.combinedFileName = realFileName;

            var fileDataParts = [content1, content2];
        
            polyPoints = convertShareToPoints(fileDataParts, settingsFileData.length, settingsFileData.radix);
            y_intercepts = solvePolys(polyPoints, settingsFileData.prime_p);
            realFileData = displaySecret(y_intercepts);

            $scope.combinedFileData = realFileData;
            $scope.combineStatus = 'File combined successfully!';
        }
        catch(e) {
            $scope.combineStatus = 'Files could not be combined - check you have uploaded the correct files';
        }
        $scope.$apply();
    }

    function readFileContent(file) {
        const reader = new FileReader()
        return new Promise((resolve, reject) => {
            reader.onload = event => resolve(event.target.result);
            reader.onerror = error => reject(error);
            reader.readAsText(file);
        });
    }
});
