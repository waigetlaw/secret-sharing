Implementation of [Shamir's secret sharing algorithm](https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing)

Deployed website: [https://waigetlaw.gitlab.io/secret-sharing](https://waigetlaw.gitlab.io/secret-sharing)